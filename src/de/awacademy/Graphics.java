package de.awacademy;

import de.awacademy.model.*;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Graphics {

    private GraphicsContext gc;
    private Model model;

    private Image spielerUp = new Image (new FileInputStream("src/de/awacademy/images/PlayerUp.png"));
    private Image spielerDown = new Image (new FileInputStream("src/de/awacademy/images/PlayerDown.png"));
    private Image spielerLeft = new Image (new FileInputStream("src/de/awacademy/images/PlayerLeft.png"));
    private Image spielerRight = new Image (new FileInputStream("src/de/awacademy/images/PlayerRight.png"));
    private Image enemyUp = new Image (new FileInputStream("src/de/awacademy/images/EnemyUp.png"));
    private Image enemyDown = new Image (new FileInputStream("src/de/awacademy/images/EnemyDown.png"));
    private Image enemyLeft = new Image (new FileInputStream("src/de/awacademy/images/EnemyLeft.png"));
    private Image enemyRight = new Image (new FileInputStream("src/de/awacademy/images/EnemyRight.png"));
    private Image spielerDead = new Image (new FileInputStream("src/de/awacademy/images/PlayerDead.png"));
    private Image life = new Image (new FileInputStream("src/de/awacademy/images/Life.png"));
    private Image arrowUp = new Image (new FileInputStream("src/de/awacademy/images/ArrowUp.png"));
    private Image arrowDown = new Image (new FileInputStream("src/de/awacademy/images/ArrowDown.png"));
    private Image arrowLeft = new Image (new FileInputStream("src/de/awacademy/images/ArrowLeft.png"));
    private Image arrowRight = new Image (new FileInputStream("src/de/awacademy/images/ArrowRight.png"));
    private Image rock = new Image (new FileInputStream("src/de/awacademy/images/Rocks.png"));
    private Image grass1 = new Image (new FileInputStream("src/de/awacademy/images/Grass1.png"));
    private Image grass2 = new Image (new FileInputStream("src/de/awacademy/images/Grass2.png"));
    private Image tree1 = new Image (new FileInputStream("src/de/awacademy/images/Tree1.png"));
    private Image tree2 = new Image (new FileInputStream("src/de/awacademy/images/Tree2.png"));

    public Graphics (GraphicsContext gc, Model model) throws FileNotFoundException {
        this.gc = gc;
        this.model = model;
    }


    public void draw() throws FileNotFoundException {
        gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);


        for (int i = 0; i < 24; i++) {
            for (int j = 0; j < 24; j++) {
                switch (model.getSpielfeld()[i][j].getImage()){
                    case "Rock" : gc.drawImage(rock, i *25, j*25); break;
                    case "Grass1" : gc.drawImage(grass1, i *25, j*25); break;
                    case "Grass2" : gc.drawImage(grass2, i *25, j*25); break;
                    case "Tree1" : gc.drawImage(tree1, i *25, j*25); break;
                    case "Tree2" : gc.drawImage(tree2, i *25, j*25); break;
                }
            }
        }
        for (int i = 0; i < model.getLeben(); i++) {
            gc.drawImage(life, 0, i*25);
        }
        if (model.getDeadenemys() != null) {
            Image enemyDead = new Image (new FileInputStream("src/de/awacademy/images/EnemyDead.png"));
            for (Enemy enemy : model.getDeadenemys()) {
                gc.drawImage(enemyDead, enemy.getX(), enemy.getY()); break;
            }
        }
        if (!(model.isGameover())) {
            switch (model.getPlayer().getRichtung()) {
                case Up: gc.drawImage(spielerUp, model.getPlayer().getX(), model.getPlayer().getY()); break;
                case Down: gc.drawImage(spielerDown, model.getPlayer().getX(), model.getPlayer().getY()); break;
                case Left: gc.drawImage(spielerLeft, model.getPlayer().getX(), model.getPlayer().getY()); break;
                case Right: gc.drawImage(spielerRight, model.getPlayer().getX(), model.getPlayer().getY()); break;
            }
        } else gc.drawImage(spielerDead, model.getPlayer().getX(), model.getPlayer().getY());

        if (model.getEnemys() != null) {
            for (Enemy enemy : model.getEnemys()) {
                switch (enemy.getRichtung()){
                    case Up: gc.drawImage(enemyUp, enemy.getX(), enemy.getY()); break;
                    case Down: gc.drawImage(enemyDown, enemy.getX(), enemy.getY()); break;
                    case Left: gc.drawImage(enemyLeft, enemy.getX(), enemy.getY()); break;
                    case Right: gc.drawImage(enemyRight, enemy.getX(), enemy.getY()); break;
                }
            }
        }




        if (model.isGameover()) {
            gc.setFill(Color.BLACK);
            gc.setFont(new Font(50 ));
            gc.fillText("Game Over", 180,
                    300
            );
        }

        if (model.isGameWon()) {
            gc.setFill(Color.BLACK);
            gc.setFont(new Font(50 ));
            gc.fillText("You Win", 200,
                    300
            );
        }



        if (model.getArrows() != null) {
            gc.setFill(Color.BROWN);
            for (Arrow arrow : model.getArrows()) {
                switch (arrow.getRichtung()) {
                    case Up: gc.drawImage(arrowUp, arrow.getX(), arrow.getY()); break;
                    case Down: gc.drawImage(arrowDown, arrow.getX(), arrow.getY()); break;
                    case Left: gc.drawImage(arrowLeft, arrow.getX(), arrow.getY()); break;
                    case Right: gc.drawImage(arrowRight, arrow.getX(), arrow.getY()); break;
                }
            }
        }

        if (model.getEnemyarrows() != null) {
            gc.setFill(Color.BROWN);
            for (Arrow earrow : model.getEnemyarrows()) {
                switch (earrow.getRichtung()) {
                    case Up: gc.drawImage(arrowUp, earrow.getX(), earrow.getY()); break;
                    case Down: gc.drawImage(arrowDown, earrow.getX(), earrow.getY()); break;
                    case Left: gc.drawImage(arrowLeft, earrow.getX(), earrow.getY()); break;
                    case Right: gc.drawImage(arrowRight, earrow.getX(), earrow.getY()); break;
                }
            }
        }
        gc.setFill(Color.WHITE);
        gc.setFont(new Font(12 ));
        gc.fillText("Punkte: " + model.getCounter(), 530,
                580
        );
        gc.setFont(new Font(18 ));
        gc.fillText("Level: " + model.getLevel(), 530,
                30
        );

    }

}
