package de.awacademy.model;

public class Player extends Figur{

    private Direction richtung = Direction.Up;
    private int x;
    private int y;
    private int h;
    private int w;
    private boolean moving = false;

    public Player(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 34;
        this.w = 22;
    }


    public void move() {

        if (isMoving()) {
            switch (richtung) {
                case Up:
                    if (this.y - 10 >= 0) {
                        this.y -= 4;
                    } break;
                case Left:
                    if (this.x - 10 >= 0) {
                        this.x -= 4;
                    } break;
                case Down:
                    if (this.y + 10 <= 570) {
                        this.y += 4;
                    } break;
                case Right:
                    if (this.x + 10 <= 570) {
                        this.x += 4;
                    } break;
            }
        }
    }

    public void setRichtung(Direction richtung){
        this.richtung = richtung;
    }

    public Direction getRichtung() {
        return richtung;
    }

    public boolean isMoving() {
        return moving;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }


}
