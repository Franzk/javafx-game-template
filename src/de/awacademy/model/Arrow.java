package de.awacademy.model;

public class Arrow extends Figur{
    private Direction richtung;
    private int x;
    private int y;
    private int h;
    private int w;

    public Arrow(int x, int y, Direction richtung) {
        this.x = x;
        this.y = y;
        this.h = 10;
        this.w = 10;
        this.richtung = richtung;
    }
    public void move() {
        switch (richtung){
            case Up : this.y -= 10; break;
            case Down : this.y += 10; break;
            case Left : this.x -= 10; break;
            case Right : this.x += 10; break;
        }
    }

    public Direction getRichtung() {
        return richtung;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }


}