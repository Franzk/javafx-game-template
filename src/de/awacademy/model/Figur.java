package de.awacademy.model;

public abstract class Figur {

    private Direction richtung;
    private int x;
    private int y;
    private int h;
    private int w;

    public Figur(){}

    public void move(int dx, int dy) {
        if (this.x+dx >= 0 && this.y+dy >= 0 && this.x+dx <= 570 && this.y+dy <= 570)  {
            this.x += dx;
            this.y += dy;
        }
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }


}