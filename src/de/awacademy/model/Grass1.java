package de.awacademy.model;

public class Grass1 extends WorldPiece {

    private int x;
    private int y;
    private int h = 25;
    private int w = 25;
    private boolean walkable = true;
    private String image = "Grass1";

    public Grass1(int x, int y){
        this.x = x;
        this.y = y;
    }

    public boolean isWalkable() {
        return walkable;
    }

    public String getImage() {
        return image;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

}