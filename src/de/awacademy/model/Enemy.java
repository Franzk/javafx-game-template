package de.awacademy.model;

public class Enemy extends Figur {
    private Direction richtung = Direction.Up;
    private int x;
    private int y;
    private int h;
    private int w;

    public Enemy(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 22;
        this.w = 34;
    }

    public void move(int dx, int dy) {
        if (this.x+dx >= 0 && this.y+dy >= 0 && this.x+dx <= 570 && this.y+dy <= 570)  {
            this.x += dx;
            this.y += dy;
        }
        if (dx < 0){this.richtung = Direction.Left;}
        if (dx > 0){this.richtung = Direction.Right;}
        if (dy < 0){this.richtung = Direction.Up;}
        if (dy > 0){this.richtung = Direction.Down;}
    }

    public Direction getRichtung() {
        return richtung;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }


}
