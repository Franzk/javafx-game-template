package de.awacademy.model;

import java.util.ArrayList;
import java.util.Random;

public class Model {
    private ArrayList<Arrow> enemyarrows = new ArrayList<>();
    private ArrayList<Arrow> arrows = new ArrayList<>();
    private ArrayList<Enemy> enemys = new ArrayList<>();
    private Player player;
    private boolean gameover = false;
    private boolean gameWon = false;
    private int counter = 0;
    private int level = 1;
    private int leben = 3;
    private Random random = new Random();
    private WorldPiece[][] spielfeld = new WorldPiece[24][24];
    private Random randomseed = new Random();
    public final double WIDTH = 600;
    public final double HEIGHT = 600;
    private ArrayList<Enemy> deadenemys = new ArrayList<>();
    private Thread enemydiesThread;

    public void enemydies(Enemy enemy) {
        enemydiesThread = new Thread(()->{
            deadenemys.add(enemy);
            enemys.remove(enemy);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            deadenemys.remove(enemy);
        });
        enemydiesThread.start();
    }

    public Model(){
        generatelevel(25, 70, 70);
    }

    public void generatelevel(int seed, int playerposx, int playerposy){
        randomseed.setSeed(seed);
        for (int i = 0; i < spielfeld.length; i++) {
            for (int j = 0; j < spielfeld.length; j++) {
                switch (randomseed.nextInt(16)) {
                    case 0: case 1: case 2: case 3: case 4: case 5: case 6: this.spielfeld[i][j] = new Grass1(i * 25, j * 25); break;
                    case 7: case 8: case 9: case 10: case 11: case 12: this.spielfeld[i][j] = new Grass2(i * 25, j * 25); break;
                    case 13: this.spielfeld[i][j] = new Tree1(i * 25, j * 25); break;
                    case 14: this.spielfeld[i][j] = new Tree2(i * 25, j * 25); break;
                    case 15: this.spielfeld[i][j] = new Rock(i * 25, j * 25); break;
                }
            }
        }
        this.player = new Player(playerposx, playerposy);
        this.enemys.add(new Enemy(randomseed.nextInt(570), randomseed.nextInt(570)));
        this.enemys.add(new Enemy(randomseed.nextInt(570), randomseed.nextInt(570)));
        this.enemys.add(new Enemy(randomseed.nextInt(570), randomseed.nextInt(570)));
    }


    public Player getPlayer() {
        return player;
    }
    public ArrayList<Enemy> getDeadenemys(){
        return deadenemys;
    }

    public ArrayList<Enemy> getEnemys() {
        return enemys;
    }

    public WorldPiece[][] getSpielfeld() {
        return spielfeld;
    }

    public ArrayList<Arrow> getEnemyarrows() {
        return enemyarrows;
    }

    public ArrayList<Arrow> getArrows() {
        return arrows;
    }

    public boolean isGameWon() {
        return gameWon;
    }

    public boolean isGameover() {
        return gameover;
    }

    public int getLevel() {
        return level;
    }

    public int getLeben() {
        return leben;
    }

    public int getCounter() {
        return this.counter;
    }

    public void shootArrow() {
        this.arrows.add(new Arrow(player.getX(), player.getY(), player.getRichtung()));
    }

    public void restart() {
        this.arrows.clear();
        this.enemyarrows.clear();
        this.enemys.clear();
        generatelevel(25, 70, 70);
        this.gameWon = false;
        this.gameover = false;
        this.counter = 0;
        this.level = 1;
        this.leben =3;
        player.setMoving(false);
    }

    public boolean collisionDetection(Figur figur1, Figur figur2) {
        return (figur1.getX() <= figur2.getX() + figur2.getH() && figur1.getX() + figur1.getH() >= figur2.getX()) &&
                (figur1.getY() <= figur2.getY() + figur2.getW() && figur1.getY() + figur1.getW() >= figur2.getY());
    }

    public void update(long nowNano){
        if (enemys != null) {
            for (Enemy enemy : enemys) {
                if (collisionDetection(player, enemy)) {
                    gameover = true;
                }
            }
        }
        player.move();
        if (enemys.isEmpty()) {
            try {
                enemydiesThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            switch (level){
                case 10 : gameWon = true; break;
                case 1 : generatelevel(50, 560, 140); this.level += 1; arrows.clear(); enemyarrows.clear(); break;
                case 2 : generatelevel(150, 30, 540); this.level += 1; arrows.clear(); enemyarrows.clear(); break;
                case 3 : generatelevel(200, 330, 30); this.level += 1; arrows.clear(); enemyarrows.clear(); break;
                case 4 : generatelevel(350, 30, 550); this.level += 1; arrows.clear(); enemyarrows.clear(); break;
                case 5 : generatelevel(450, 550, 300); this.level += 1; arrows.clear(); enemyarrows.clear(); break;
                case 6 : generatelevel(550, 550, 100); this.level += 1; arrows.clear(); enemyarrows.clear(); break;
                case 7 : generatelevel(650, 50, 300); this.level += 1; arrows.clear(); enemyarrows.clear(); break;
                case 8 : generatelevel(750, 100, 500); this.level += 1; arrows.clear(); enemyarrows.clear(); break;
                case 9 : generatelevel(850, 300, 80); this.level += 1; arrows.clear(); enemyarrows.clear(); break;
            }
        }

        if (this.arrows != null) {
            for (Arrow arrow : arrows) {
                arrow.move();
            }
        }
        if (this.enemyarrows != null) {
            for (Arrow earrow : enemyarrows) {
                earrow.move();
            }
        }
        if (this.arrows != null && this.enemys != null) {
            try {
                for (Arrow arrow : arrows) {
                    for (Enemy enemy : enemys) {
                        if (collisionDetection(arrow, enemy)) {
                            counter += 1;
                            arrows.remove(arrow);
                            enemydies(enemy);
                            break;
                        }
                    }
                }
            } catch (Exception e) { }
        }

        if (this.enemyarrows != null) {
            try {
                for (Arrow earrow : enemyarrows) {
                    if (collisionDetection(earrow, player)) {
                        enemyarrows.remove(earrow);
                        this.leben -= 1;
                        if (leben == 0) this.gameover = true;
                    }
                }
            } catch (Exception e) { }
        }
        if (this.enemys != null) {
            for (Enemy enemy : enemys) {
                if (random.nextInt(10) == 5) {
                    if (random.nextBoolean()) {
                        enemy.move(((random.nextInt(3) - 1) * 10), 0);
                    } else enemy.move(0, ((random.nextInt(3) - 1)) * 10);
                }
                if (random.nextInt(50) == 10) {
                    this.enemyarrows.add(new Arrow(enemy.getX(), enemy.getY(), enemy.getRichtung()));
                }
            }
        }
    }
}
