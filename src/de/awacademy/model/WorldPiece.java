package de.awacademy.model;

import javafx.scene.image.Image;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public abstract class WorldPiece {

    private int x;
    private int y;
    private int h = 25;
    private int w = 25;
    private boolean walkable;
    private String path;
    private String image;

    public WorldPiece() {
    }

    public WorldPiece(int x, int y, boolean walkable) {
        this.x = x;
        this.y = y;
        this.walkable = walkable;
    }

    public boolean isWalkable() {
        return walkable;
    }

    public String getImage() {
        return image;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }


}
