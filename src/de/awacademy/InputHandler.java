package de.awacademy;

import de.awacademy.model.Direction;
import de.awacademy.model.Model;
import javafx.scene.input.KeyCode;

public class InputHandler {

    private Model model;

    private boolean upPressed = false;
    private boolean downPressed = false;
    private boolean leftPressed = false;
    private boolean rightPressed = false;

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode keycode) {

        if (keycode == KeyCode.UP) {
            model.getPlayer().setMoving(true);
            model.getPlayer().setRichtung(Direction.Up);
            upPressed =true;
        }
        if (keycode == KeyCode.DOWN) {
            model.getPlayer().setMoving(true);
            model.getPlayer().setRichtung(Direction.Down);
            downPressed = true;
        }
        if (keycode == KeyCode.LEFT) {
            model.getPlayer().setMoving(true);
            model.getPlayer().setRichtung(Direction.Left);
            leftPressed = true;
        }
        if (keycode == KeyCode.RIGHT) {
            model.getPlayer().setMoving(true);
            model.getPlayer().setRichtung(Direction.Right);
            rightPressed = true;
        }
        if (keycode == KeyCode.A && (!(model.isGameover())) && (!(model.isGameWon()))) {
            model.shootArrow();
        }

        if (keycode == KeyCode.J && (model.isGameover() || model.isGameWon())) {
            model.restart();
        }
    }

    public void onKeyReleased(KeyCode keycode) {
        if (keycode == KeyCode.UP) {
            upPressed = false;
            if (!(upPressed) && !(downPressed) && !(leftPressed) && !(rightPressed)) model.getPlayer().setMoving(false);
        }
        if (keycode == KeyCode.DOWN) {
            downPressed = false;
            if (!(upPressed) && !(downPressed) && !(leftPressed) && !(rightPressed)) model.getPlayer().setMoving(false);
        }
        if (keycode == KeyCode.LEFT) {
            leftPressed = false;
            if (!(upPressed) && !(downPressed) && !(leftPressed) && !(rightPressed)) model.getPlayer().setMoving(false);
        }
        if (keycode == KeyCode.RIGHT) {
            rightPressed = false;
            if (!(upPressed) && !(downPressed) && !(leftPressed) && !(rightPressed)) model.getPlayer().setMoving(false);
        }
    }
}
