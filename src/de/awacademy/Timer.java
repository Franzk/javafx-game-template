package de.awacademy;

import de.awacademy.model.Model;
import javafx.animation.AnimationTimer;

import java.io.FileNotFoundException;

public class Timer extends AnimationTimer {

    private Graphics graphics;
    private Model model;

    public Timer(Graphics graphics, Model model) {
        this.graphics = graphics;
        this.model = model;
    }

    @Override
    public void handle(long nowNano) {
        if (!(model.isGameover()) && !(model.isGameWon())) {
                model.update(nowNano);
        }
        try {
            graphics.draw();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
